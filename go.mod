module gitlab.com/gotoko-case-study/coffeesvc

go 1.16

require (
	github.com/gogo/gateway v1.1.0
	github.com/gogo/googleapis v1.4.1
	github.com/gogo/protobuf v1.3.2
	github.com/golang/mock v1.1.1
	github.com/golang/protobuf v1.5.2
	github.com/googleapis/googleapis v0.0.0-20211130235732-d78fa54f7a39 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.10
	github.com/mkideal/cli v0.0.5 // indirect
	github.com/mwitkow/go-proto-validators v0.3.2
	github.com/rakyll/statik v0.1.7
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.26.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect

)
