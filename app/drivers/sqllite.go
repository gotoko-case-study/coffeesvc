package drivers

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

func SQLLiteDBInit(dataSourceName string) (*sql.DB, error) {
	// setup db source
	db, err := sql.Open("sqlite3", dataSourceName)
	if err != nil {
		return nil, err
	}

	// run database setup for table coffee_menus
	statement, err := db.Prepare("CREATE TABLE IF NOT EXISTS coffee_menus (" +
		"id INTEGER PRIMARY KEY, " +
		"name VARCHAR(250), " +
		"beans_qty INT8, " +
		"stock INT8, " +
		"sold INT8, " +
		"available INT8" +
		")")
	if err != nil {
		return nil, err
	}

	_, err = statement.Exec()
	if err != nil {
		return nil, err
	}

	// initial insert menu
	statement, err = db.Prepare("INSERT INTO coffee_menus (name, beans_qty, stock, sold, available) VALUES " +
		"('LATTE', 50, 10, 0, 10), " +
		"('CAPPUCINO', 20, 10, 0, 10), " +
		"('CAFFE MOCHA', 30, 10, 0, 10), " +
		"('AFFOGATO', 10, 10, 0, 10), " +
		"('FLAT WHITE', 40, 10, 0, 10);")
	if err != nil {
		return nil, err
	}

	_, err = statement.Exec()
	if err != nil {
		return nil, err
	}

	// run database setup for table temp_coffee_beans
	statement, err = db.Prepare("CREATE TABLE IF NOT EXISTS temp_coffee_beans (" +
		"id INTEGER PRIMARY KEY, " +
		"beans VARCHAR(250), " +
		"stock INT8" +
		")")
	if err != nil {
		return nil, err
	}

	_, err = statement.Exec()
	if err != nil {
		return nil, err
	}

	// run database setup for table coffee_orders
	statement, err = db.Prepare("CREATE TABLE IF NOT EXISTS coffee_orders (" +
		"id INTEGER PRIMARY KEY, " +
		"menu_id INT8, " +
		"beans_id INT8, " +
		"beans_qty INT8, " +
		"quantity INT8, " +
		"grind_size VARCHAR(250), " +
		"serve_type VARCHAR(250), " +
		"status VARCHAR(250), " +
		"notes TEXT, " +
		"created_at TIMESTAMPZ" +
		")")
	if err != nil {
		return nil, err
	}

	_, err = statement.Exec()
	if err != nil {
		return nil, err
	}

	return db, nil
}
