package usecases

import (
	"context"
	"math/rand"
	"net/http"

	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/entities"
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/repositories/sqllite/temp_coffee_beans"
	pb "gitlab.com/gotoko-case-study/coffeesvc/proto"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/status"
)

type GetCoffeeBeansUseCase interface {
	GetCoffeeBeans(ctx context.Context) ([]pb.CoffeeBeansData, error)
}

type GetCoffeeBeansImpl struct {
	logger          grpclog.LoggerV2
	tempCoffeeBeans temp_coffee_beans.ITempCoffeeBeansRepository
}

func NewGetCoffeeBeansUseCase(logger grpclog.LoggerV2, tempCoffeeBeans temp_coffee_beans.ITempCoffeeBeansRepository) *GetCoffeeBeansImpl {
	return &GetCoffeeBeansImpl{
		logger:          logger,
		tempCoffeeBeans: tempCoffeeBeans,
	}
}

func (s GetCoffeeBeansImpl) GetCoffeeBeans(ctx context.Context) ([]pb.CoffeeBeansData, error) {
	// flush beans data and replace from third party data
	// for now, response third party is hardcode
	err := s.tempCoffeeBeans.Flush()
	if err != nil {
		s.logger.Error("Error when delete temporary beans data ", err)
		return nil, status.Error(http.StatusInternalServerError, "Internal Server Error")
	}

	// re-insert beans data with hardcoded data
	// this hardcoded data will be replaced with third party response
	tempBeans := []entities.TempCoffeeBeans{
		{
			Id:    1,
			Beans: "Mandailing",
			Stock: s.randStock(),
		},
		{
			Id:    2,
			Beans: "Gayo",
			Stock: s.randStock(),
		},
		{
			Id:    3,
			Beans: "Honey Gayo",
			Stock: s.randStock(),
		},
	}
	for _, tempBean := range tempBeans {
		err = s.tempCoffeeBeans.Save(tempBean)
		if err != nil {
			s.logger.Error("Error when inserting temporary beans data ", err)
			return nil, status.Error(http.StatusInternalServerError, "Internal Server Error")
		}
	}

	// get temp beans data
	beans, err := s.tempCoffeeBeans.GetAvailableBeans()
	if err != nil {
		s.logger.Error("Error when fetch temporary beans data ", err)
		return nil, status.Error(http.StatusInternalServerError, "Internal Server Error")
	}

	var resp []pb.CoffeeBeansData

	for _, bean := range beans {
		data := pb.CoffeeBeansData{
			Id:    bean.Id,
			Beans: bean.Beans,
			Stock: bean.Stock,
		}
		resp = append(resp, data)
	}

	return resp, nil
}

func (n GetCoffeeBeansImpl) randStock() uint64 {
	max := 500
	min := 100
	return uint64(rand.Intn(max-min) + min)
}
