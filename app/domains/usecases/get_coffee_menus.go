package usecases

import (
	"context"
	"net/http"

	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/repositories/sqllite/coffee_menu"
	pb "gitlab.com/gotoko-case-study/coffeesvc/proto"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/status"
)

type GetCoffeeMenusUseCase interface {
	GetCoffeeMenus(ctx context.Context) ([]pb.CoffeeMenusData, error)
}

type GetCoffeeMenusImpl struct {
	logger         grpclog.LoggerV2
	coffeeMenuRepo coffee_menu.ICoffeeMenuRepository
}

func NewGetCoffeeMenusUseCase(logger grpclog.LoggerV2, coffeeMenuRepo coffee_menu.ICoffeeMenuRepository) *GetCoffeeMenusImpl {
	return &GetCoffeeMenusImpl{
		logger:         logger,
		coffeeMenuRepo: coffeeMenuRepo,
	}
}

func (s GetCoffeeMenusImpl) GetCoffeeMenus(ctx context.Context) ([]pb.CoffeeMenusData, error) {
	// get temp menus data
	menus, err := s.coffeeMenuRepo.GetAvailableMenu()
	if err != nil {
		s.logger.Error("Error when fetch temporary menus data ", err)
		return nil, status.Error(http.StatusInternalServerError, "Internal Server Error")
	}

	var resp []pb.CoffeeMenusData

	for _, menu := range menus {
		data := pb.CoffeeMenusData{
			Id:             menu.Id,
			Menu:           menu.Name,
			AvailableStock: menu.Available,
		}
		resp = append(resp, data)
	}

	return resp, nil
}
