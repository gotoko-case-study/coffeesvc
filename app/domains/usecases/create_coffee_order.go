package usecases

import (
	"context"
	"net/http"

	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/entities"
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/repositories/sqllite/coffee_menu"
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/repositories/sqllite/coffee_order"
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/repositories/sqllite/temp_coffee_beans"
	pb "gitlab.com/gotoko-case-study/coffeesvc/proto"
	"google.golang.org/grpc/grpclog"
	"google.golang.org/grpc/status"
)

type CreateCoffeeOrderUseCase interface {
	CreateCoffeeOrder(ctx context.Context, req *pb.CoffeeOrderRequest) (string, error)
}

type CreateCoffeeOrderImpl struct {
	logger          grpclog.LoggerV2
	coffeeOrder     coffee_order.ICoffeeOrderRepository
	coffeeMenu      coffee_menu.ICoffeeMenuRepository
	tempCoffeeBeans temp_coffee_beans.ITempCoffeeBeansRepository
}

func NewCreateCoffeeOrderUseCase(logger grpclog.LoggerV2, coffeeOrder coffee_order.ICoffeeOrderRepository, coffeeMenu coffee_menu.ICoffeeMenuRepository, tempCoffeeBeans temp_coffee_beans.ITempCoffeeBeansRepository) *CreateCoffeeOrderImpl {
	return &CreateCoffeeOrderImpl{
		logger:          logger,
		coffeeMenu:      coffeeMenu,
		coffeeOrder:     coffeeOrder,
		tempCoffeeBeans: tempCoffeeBeans,
	}
}

func (s CreateCoffeeOrderImpl) CreateCoffeeOrder(ctx context.Context, req *pb.CoffeeOrderRequest) (string, error) {
	// get menu detail
	menu, err := s.coffeeMenu.GetDetailMenu(req.MenuId)
	if err != nil {
		s.logger.Error("Error when get detail menu ", err)
		return "", status.Error(http.StatusInternalServerError, "Internal Server Error")
	}

	// get detail beans
	beans, err := s.tempCoffeeBeans.GetDetailBeans(req.BeansId)
	if err != nil {
		s.logger.Error("Error when get detail beans ", err)
		return "", status.Error(http.StatusInternalServerError, "Internal Server Error")
	}

	// check available menu with qty
	if menu.Available < req.Qty {
		return "", status.Error(http.StatusBadRequest, "Menu is out of stock")
	}

	// check beans stock, qty * beans stock
	beansNeeded := menu.BeansQty * req.Qty
	if beans.Stock < beansNeeded {
		return "", status.Error(http.StatusBadRequest, "Beans is out of stock")
	}

	// check cupboard if take away
	// this variable should replace to call to another third party to get availability cupboard
	// for now this logic will always return true
	cardboardCupIsAvailable := true
	if req.ServeType == pb.CoffeeBeansServeType_TAKE_AWAY && !cardboardCupIsAvailable {
		return "", status.Error(http.StatusBadRequest, "Beans is out of stock")
	}

	// serve menu step by step
	s.logger.Infoln("Start Serving Menu " + menu.Name)
	s.logger.Infoln("Grind Beans " + beans.Beans + " With Grind level " + req.GrindLevel.String())
	s.logger.Infoln("Make Espresso")
	s.logger.Infoln("Make Latte")

	serveCup := "a coffee cup glass"
	if req.ServeType == pb.CoffeeBeansServeType_TAKE_AWAY {
		serveCup = "a take away cardboard cup"
	}

	s.logger.Infoln("Serve Latte to " + serveCup)

	s.logger.Infoln("Coffee Served to beloved customer <3")

	order := entities.CoffeeOrder{
		MenuId:    req.MenuId,
		BeansId:   req.BeansId,
		BeansQty:  beansNeeded,
		Quantity:  req.Qty,
		GrindSize: req.GrindLevel.String(),
		ServeType: req.ServeType.String(),
		Status:    "completed",
		Notes:     "Coffee Served to beloved customer <3",
	}
	err = s.coffeeOrder.Save(order)
	if err != nil {
		s.logger.Error("Error when saving order", err)
		return "", status.Error(http.StatusInternalServerError, "Internal Server Error")
	}
	// hit thirdparty beans to update their stock if needed

	// update availability menu
	err = s.coffeeMenu.UpdateAvailableMenu(req.MenuId, req.Qty)
	if err != nil {
		s.logger.Error("Error when update availability menu ", err)
		return "", status.Error(http.StatusInternalServerError, "Internal Server Error")
	}

	return "Coffee Served to beloved customer <3", nil
}
