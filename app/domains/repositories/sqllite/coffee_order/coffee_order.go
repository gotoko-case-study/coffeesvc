package coffee_order

import (
	"database/sql"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/entities"
)

type CoffeeOrderRepository struct {
	db *sql.DB
}

func NewCoffeeOrderRepository(db *sql.DB) *CoffeeOrderRepository {
	return &CoffeeOrderRepository{
		db: db,
	}
}

func (n CoffeeOrderRepository) Save(data entities.CoffeeOrder) error {
	query, _ := n.db.Prepare("INSERT INTO coffee_orders " +
		"VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
	_, err := query.Exec(data.MenuId, data.BeansId, data.BeansQty, data.Quantity, data.GrindSize, data.ServeType, data.Status, data.Notes, time.Now())
	if err != nil {
		return err
	}

	return nil
}
