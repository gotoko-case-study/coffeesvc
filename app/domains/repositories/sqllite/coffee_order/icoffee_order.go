package coffee_order

import (
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/entities"
)

type ICoffeeOrderRepository interface {
	Save(data entities.CoffeeOrder) error
}
