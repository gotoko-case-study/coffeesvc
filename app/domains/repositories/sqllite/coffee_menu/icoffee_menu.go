package coffee_menu

import (
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/entities"
)

type ICoffeeMenuRepository interface {
	UpdateAvailableMenu(id, sold uint64) error
	GetAvailableMenu() ([]entities.CoffeeMenu, error)
	GetDetailMenu(id uint64) (entities.CoffeeMenu, error)
}
