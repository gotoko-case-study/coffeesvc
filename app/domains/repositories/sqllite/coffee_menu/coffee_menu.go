package coffee_menu

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/entities"
)

type CoffeeMenuRepository struct {
	db *sql.DB
}

func NewCoffeeMenuRepository(db *sql.DB) *CoffeeMenuRepository {
	return &CoffeeMenuRepository{
		db: db,
	}
}

func (n CoffeeMenuRepository) GetAvailableMenu() ([]entities.CoffeeMenu, error) {
	var menus []entities.CoffeeMenu
	query, err := n.db.Prepare("SELECT id, name, beans_qty, stock, sold, available " +
		"FROM coffee_menus " +
		"WHERE available > 0 " +
		"ORDER BY id ASC ")
	if err != nil {
		return nil, err
	}
	defer query.Close()

	rows, err := query.Query()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		menu := entities.CoffeeMenu{}
		err = rows.Scan(&menu.Id, &menu.Name, &menu.BeansQty, &menu.Stock, &menu.Sold, &menu.Available)
		if err != nil {
			return nil, err
		}

		menus = append(menus, menu)
	}

	return menus, nil
}

func (n CoffeeMenuRepository) GetDetailMenu(id uint64) (entities.CoffeeMenu, error) {
	menu := entities.CoffeeMenu{}
	query, err := n.db.Prepare("SELECT id, name, beans_qty, stock, sold, available " +
		"FROM coffee_menus " +
		"WHERE id = ? ")
	if err != nil {
		return menu, err
	}
	defer query.Close()

	rows, err := query.Query(id)
	if err != nil {
		return menu, err
	}

	for rows.Next() {
		err = rows.Scan(&menu.Id, &menu.Name, &menu.BeansQty, &menu.Stock, &menu.Sold, &menu.Available)
		if err != nil {
			return menu, err
		}
	}

	return menu, nil
}
func (n CoffeeMenuRepository) UpdateAvailableMenu(id, sold uint64) error {
	query, _ := n.db.Prepare("UPDATE coffee_menus set available = available - ?, sold = sold + ? WHERE id = ?")
	_, err := query.Exec(sold, sold, id)
	if err != nil {
		return err
	}

	return nil
}
