package temp_coffee_beans

import (
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/entities"
)

type ITempCoffeeBeansRepository interface {
	Save(data entities.TempCoffeeBeans) error
	GetAvailableBeans() ([]entities.TempCoffeeBeans, error)
	GetDetailBeans(id uint64) (entities.TempCoffeeBeans, error)
	Flush() error
}
