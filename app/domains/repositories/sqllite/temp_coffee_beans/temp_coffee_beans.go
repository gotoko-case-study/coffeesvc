package temp_coffee_beans

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/entities"
)

type TempCoffeeBeansRepository struct {
	db *sql.DB
}

func NewTempCoffeeBeansRepository(db *sql.DB) *TempCoffeeBeansRepository {
	return &TempCoffeeBeansRepository{
		db: db,
	}
}

func (n TempCoffeeBeansRepository) Save(data entities.TempCoffeeBeans) error {
	query, _ := n.db.Prepare("INSERT INTO temp_coffee_beans " +
		"VALUES (?, ?, ?)")
	_, err := query.Exec(data.Id, data.Beans, data.Stock)
	if err != nil {
		return err
	}

	return nil
}

func (n TempCoffeeBeansRepository) GetAvailableBeans() ([]entities.TempCoffeeBeans, error) {
	var beans []entities.TempCoffeeBeans
	query, err := n.db.Prepare("SELECT id, beans, stock " +
		"FROM temp_coffee_beans " +
		"WHERE stock > 0 " +
		"ORDER BY id ASC ")
	if err != nil {
		return nil, err
	}
	defer query.Close()

	rows, err := query.Query()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		bean := entities.TempCoffeeBeans{}
		err = rows.Scan(&bean.Id, &bean.Beans, &bean.Stock)
		if err != nil {
			return nil, err
		}

		beans = append(beans, bean)
	}

	return beans, nil
}

func (n TempCoffeeBeansRepository) GetDetailBeans(id uint64) (entities.TempCoffeeBeans, error) {
	beans := entities.TempCoffeeBeans{}
	query, err := n.db.Prepare("SELECT id, beans, stock " +
		"FROM temp_coffee_beans " +
		"WHERE id = ? ")
	if err != nil {
		return beans, err
	}
	defer query.Close()

	rows, err := query.Query(id)
	if err != nil {
		return beans, err
	}

	for rows.Next() {
		err = rows.Scan(&beans.Id, &beans.Beans, &beans.Stock)
		if err != nil {
			return beans, err
		}
	}
	return beans, nil
}

func (n TempCoffeeBeansRepository) Flush() error {
	query, err := n.db.Prepare("DELETE FROM temp_coffee_beans")
	if err != nil {
		return err
	}
	defer query.Close()

	_, err = query.Exec()
	if err != nil {
		return err
	}

	return nil
}
