package entities

import "time"

type CoffeeOrder struct {
	// nolint
	Id        uint64
	MenuId    uint64
	BeansId   uint64
	BeansQty  uint64
	Quantity  uint64
	GrindSize string
	ServeType string
	Status    string
	Notes     string
	CreatedAt time.Time
}
