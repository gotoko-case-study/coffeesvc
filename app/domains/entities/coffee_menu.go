package entities

type CoffeeMenu struct {
	// nolint
	Id        uint64
	Name      string
	BeansQty  uint64
	Stock     uint64
	Sold      uint64
	Available uint64
}
