package handlers

import (
	"context"
	"net/http"

	pb "gitlab.com/gotoko-case-study/coffeesvc/proto"
)

func (b *Backend) CreateCoffeeOrder(ctx context.Context, req *pb.CoffeeOrderRequest) (*pb.Response, error) {
	resp, err := b.CoffeeOrderUseCase.CreateCoffeeOrder(ctx, req)
	if err != nil {
		return nil, err
	}

	return &pb.Response{
		Status:  http.StatusCreated,
		Message: resp,
	}, nil
}
