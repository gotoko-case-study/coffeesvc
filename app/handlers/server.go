package handlers

import (
	"database/sql"
	"io/ioutil"
	"os"
	"sync"

	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/repositories/sqllite/coffee_menu"
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/repositories/sqllite/coffee_order"
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/repositories/sqllite/temp_coffee_beans"
	"gitlab.com/gotoko-case-study/coffeesvc/app/domains/usecases"
	"gitlab.com/gotoko-case-study/coffeesvc/app/drivers"
	pb "gitlab.com/gotoko-case-study/coffeesvc/proto"
	"google.golang.org/grpc/grpclog"
)

type Backend struct {
	mu                    *sync.RWMutex
	db                    *sql.DB
	Logger                grpclog.LoggerV2
	CoffeeMenuRepo        coffee_menu.ICoffeeMenuRepository
	TempCoffeeBeansRepo   temp_coffee_beans.ITempCoffeeBeansRepository
	CoffeeOrderRepo       coffee_order.ICoffeeOrderRepository
	CoffeeOrderUseCase    usecases.CreateCoffeeOrderUseCase
	GetCoffeeBeansUseCase usecases.GetCoffeeBeansUseCase
	GetCoffeeMenusUseCase usecases.GetCoffeeMenusUseCase
}

var _ pb.CoffeeServer = (*Backend)(nil)

var dbPath = "./digital_coffee.db"

func NewServer() *Backend {
	var log grpclog.LoggerV2
	log = grpclog.NewLoggerV2(os.Stdout, ioutil.Discard, ioutil.Discard)
	grpclog.SetLoggerV2(log)

	server := &Backend{
		mu:     &sync.RWMutex{},
		Logger: log,
	}

	sqlLiteDB, err := drivers.SQLLiteDBInit(dbPath)
	if err != nil {
		log.Fatalln("Failed to create sqllite DB", err)
		return nil
	}

	server.db = sqlLiteDB

	server.CoffeeMenuRepo = coffee_menu.NewCoffeeMenuRepository(server.db)
	server.TempCoffeeBeansRepo = temp_coffee_beans.NewTempCoffeeBeansRepository(server.db)
	server.CoffeeOrderRepo = coffee_order.NewCoffeeOrderRepository(server.db)

	server.CoffeeOrderUseCase = usecases.NewCreateCoffeeOrderUseCase(server.Logger, server.CoffeeOrderRepo, server.CoffeeMenuRepo, server.TempCoffeeBeansRepo)
	server.GetCoffeeBeansUseCase = usecases.NewGetCoffeeBeansUseCase(server.Logger, server.TempCoffeeBeansRepo)
	server.GetCoffeeMenusUseCase = usecases.NewGetCoffeeMenusUseCase(server.Logger, server.CoffeeMenuRepo)

	return server
}
