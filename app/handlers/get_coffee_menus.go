package handlers

import (
	"context"

	pb "gitlab.com/gotoko-case-study/coffeesvc/proto"
)

func (b *Backend) GetCoffeeMenus(ctx context.Context, req *pb.RequestList) (*pb.CoffeeMenusResponse, error) {
	data, err := b.GetCoffeeMenusUseCase.GetCoffeeMenus(ctx)
	if err != nil {
		return nil, err
	}

	rsp := &pb.CoffeeMenusResponse{
		Meta: &pb.ListPageResponseMeta{
			Limit:     req.Limit,
			Offset:    req.Offset,
			TotalData: uint64(len(data)),
		},
	}

	for _, menu := range data {
		pack := &pb.CoffeeMenusData{
			Id:             menu.Id,
			Menu:           menu.Menu,
			AvailableStock: menu.AvailableStock,
		}
		rsp.Data = append(rsp.Data, pack)
	}
	return rsp, nil
}
