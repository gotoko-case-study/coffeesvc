package handlers

import (
	"context"

	pb "gitlab.com/gotoko-case-study/coffeesvc/proto"
)

func (b *Backend) GetCoffeeBeans(ctx context.Context, req *pb.RequestList) (*pb.CoffeeBeansResponse, error) {
	data, err := b.GetCoffeeBeansUseCase.GetCoffeeBeans(ctx)
	if err != nil {
		return nil, err
	}

	rsp := &pb.CoffeeBeansResponse{
		Meta: &pb.ListPageResponseMeta{
			Limit:     req.Limit,
			Offset:    req.Offset,
			TotalData: uint64(len(data)),
		},
	}

	for _, bean := range data {
		pack := &pb.CoffeeBeansData{
			Id:    bean.Id,
			Beans: bean.Beans,
			Stock: bean.Stock,
		}
		rsp.Data = append(rsp.Data, pack)
	}
	return rsp, nil
}
