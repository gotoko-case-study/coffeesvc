# Go-toko Case Study - Digital Coffee

## Installation

```bash
$ git clone git@gitlab.com:gotoko-case-study/coffeesvc.git
```

## Running it

```bash
$ go build main.go
$ go run main.go
```

After starting the server, you can access the OpenAPI UI on
[https://localhost:11000/docs/](https://localhost:11000/docs/)

## Development

To regenerate the proto files, ensure you have installed the generate dependencies:

```bash
$ GO111MODULE=on make install
```

It also requires you to have the Google Protobuf compiler `protoc` installed. Please follow instructions for your
platform on the
[official protoc repo](https://github.com/google/protobuf#protocol-compiler-installation).

Regenerate the files by running `make generate`:

```bash
$ make generate
```
